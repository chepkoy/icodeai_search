.. IcodeAI Search documentation master file, created by
   sphinx-quickstart on Wed Nov 25 14:44:54 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to IcodeAI Search's documentation!
==========================================

The search service leverages `ElasticSearch <https://www.elastic.co/>`_ and provides a 
rest API to serve the requests from the UI service

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
