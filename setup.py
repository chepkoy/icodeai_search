import setuptools

setuptools.setup(
    name="search",
    version="0.0.0",
    description ="Search Service",
    packages = setuptools.find_packages('src'),
    package_dir={'':'src'},
    author="Allan Chepkoy",
    author_email = "allankiplangat22@gmail.com",
    licence="",
    install_requires=['']
)